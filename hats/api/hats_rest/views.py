from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse


# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location" : LocationVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, id = None):
    if request.method == "GET":
        if id is not None:
            hats = Hat.objects.filter(location=id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_show_location_vos(request):
    location_VOs = LocationVO.objects.all()
    locations = []
    for location in location_VOs:
        locations.append({
            "id": location.id,
            "href": location.import_href,
            "closet_name": location.closet_name,
            "section_number": location.section_number,
            "shelf_number": location.shelf_number,
        })
        
    return JsonResponse({"locations": locations})


@require_http_methods(["DELETE"])
def api_show_hat(request, id):
    count, _ = Hat.objects.get(id=id).delete()
    return JsonResponse({"deleted": count > 0})