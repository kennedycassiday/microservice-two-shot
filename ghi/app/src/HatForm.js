import React, { useState, useEffect } from 'react';

const HatForm = ({ getHats }) => {
    const [hatFabric, setHatFabric] = useState('');
    const [hatStyle, setHatStyle] = useState('');
    const [hatColor, setHatColor] = useState('');
    const [hatPicture, setHatPicture] = useState('');
    const [hatLocation, setHatLocation] = useState('');
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        const locationVOsURL = "http://localhost:8100/api/locations/"
        fetch(locationVOsURL)
            .then(response => response.json())
            .then(data => setLocations(data.locations))
            .catch(e => console.error('error: ', e))
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newHat = {
            'fabric': hatFabric,
            'style_name': hatStyle,
            'color': hatColor,
            'picture_url': hatPicture,
            'location': hatLocation,
        }

        const hatsUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newHat),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(hatsUrl, fetchConfig)
            .then(response => {
                if (response.ok) {
                    return getHats()
                }
            })

            .then(() => {
                setHatFabric('');
                setHatStyle('');
                setHatColor('');
                setHatPicture('');
                setHatLocation('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setHatFabric(value);
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setHatStyle(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setHatColor(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setHatPicture(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setHatLocation(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create hat</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={hatFabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatStyle} onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatColor} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatPicture} onChange={handlePictureChange} placeholder="Picture Url" required type="text" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture Url</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleLocationChange} value={hatLocation} required id="location" name="location" className="form-select">
                                <option value="">Choose location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
