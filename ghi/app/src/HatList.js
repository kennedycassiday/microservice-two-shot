import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom";

function HatList({ hats, getHats }) {
    const deleteHat = async (id) => {
        fetch(`http://wardrobe:8090/hats/${id}/`, {
            method: "delete",
        })
            .then(() => {
                return getHats()
            })
            .catch(console.log)
    }
    // if (props.hats === undefined) {
    //     return null
    // }

    return (
        <>
            <table className="table table-striped align-middle mt-5">
                <thead>
                    <tr>
                        <th>Hat</th>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style}</td>
                                <td><img src={hat.picture_url} className="img-thumbnail hats"></img></td>
                                <td>{hat.location.closet_name}</td>
                                <td>
                                    <button type="button" value={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/hats/new"}>Create hat</Link>
        </>
    );
}

export default HatList;