import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import { useState, useEffect } from "react";

function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])

  const getShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const shoes = data.shoes
      setShoes(shoes)
      console.log(shoes)
    }
  }

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }
  }

  useEffect(() => {
    getShoes();
    getHats();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={shoes} getShoes={getShoes} />} />
          <Route path="/shoes/new" element={<ShoeForm getShoes={getShoes} />}></Route>
          <Route path="hats" element={<HatList hats={hats} getHats={getHats} />} />
          <Route path="/hats/new" element={<HatForm getHats={getHats} />} />
        </Routes>
      </div>
    </BrowserRouter >
  );
}

export default App;
