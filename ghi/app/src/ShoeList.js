import React from "react";

function ShoeList({shoes, getShoes}) {

  const deleteShoe = async (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}/`, {
      method: "delete",
    })

    .then(() => {
      getShoes()
    })
    .catch(console.log)
  }


    return (
        <table className=" table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin Number</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td>
                    <img
                      src={ shoe.picture_url }
                      alt=""
                      width="75px"
                      height="75px"
                    />
                  </td>
                  <td>{ shoe.bin }</td>
                  <td><button type="button" className="btn btn-outline-dark" value={shoe.id} onClick={() => deleteShoe(shoe.id)} >Delete Shoe</button></td>
                </tr>
              );
           })}
          </tbody>
        </table>
    )
}

export default ShoeList;
