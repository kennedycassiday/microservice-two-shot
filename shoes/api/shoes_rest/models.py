from django.db import models

class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        "BinVO",
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,

    )
