# Wardrobify

Team:

* Kennedy Cassiday - Shoes Microservice
* Lauren Alpuerto - Hats Microservice

## Design

## Shoes microservice

The Shoes microservice has two models, Shoe and BinVO.
The Shoe model contains the following attributes: manufacturer, model name, color, picture url, and bin (bin is a Foreign Key referencing the BinVO model).The BinVO model contains the bin number and import href attributes.
The BinVO model polls the Wardrobe's Bin model for data (specifically for bin number data).


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

1. LocationVO
    - Includes import_href, closet_name, section_number, and shelf_number. This polls the wardrobe microservice's location model for data.
    
2. Hat
    - Includes fabric, style_name, color, picture_url, and location.